Select * From customers;

Update customers Set Country = replace(Country, '\n', '');
Update customers Set City = Replace(City, '\n', '');

Select CustomerID, CustomerName, ContactName From customers Where Country = "Mexico";

Create View mexicanCustomers as Select CustomerID, CustomerName, ContactName From customers Where Country = "Mexico";
Select * From mexicancustomers;

Select * From mexicancustomers Join orders on mexicancustomers.CustomerID = orders.CustomerID;

Select ProductID, ProductName, Price From products Where Price < (Select Avg(Price) From products);

Select Avg(price) From products;

Create View productsbelowavg as Select PRoductID, ProductName, Price From products Where Price < (Select Avg(Price) From products);

Delete From orderdetails Where ProductID = 5;
Truncate orderdetails; #faster than delete

Delete From customers;
Delete From orders;

Drop Table customers;

