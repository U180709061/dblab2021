use movie_db;
# 1. Show the films whose budget is greater than 10 million$ and ranking is less than 6.
	Select title From movies Where budget > 10000000 and ranking < 6;

# 2. Show the action films whose rating is greater than 8.8 and produced before 2009.
	Select title From movies join genres on movies.movie_id = genres.movie_id Where rating > 8.8 and year < 2009 and genre_name = "Action";

# 3. Show the drama films whose duration is more than 150 minutes and oscars is more than 2.
	Select title From movies join genres on movies.movie_id = genres.movie_id Where genre_name = "Drama" and duration > 150 and oscars > 2;
    Select title From movies Where duration > 150 and oscars > 2 and movie_id in (Select movie_id From genres Where genre_name = "Drama");

# 4. Show the films that Orlando Bloom and Ian McKellen have act together and has more than 2 Oscars.
	#Select title From movies Where oscars > 2 and movie_id in (Select movie_id From movie_stars Where ) and star_id in (Select star_id From stars Where star_name = 
    #"Orlando Bloom" and star_name = "Ian McKellen")
    Select title From movies join movie_stars on movies.movie_id = movie_stars.movie_id join stars on movie_stars.star_id = stars.star_id Where 
    star_name = "Orlando Bloom" and star_name = "Ian McKellen" and oscars > 2;
    Select star_id From stars Where star_name = "Orlando Bloom" or star_name = "Ian McKellen"; #24 - 25
    Select * From stars;
    Select movie_id From movie_stars Where star_id = 24 or star_id = 25; # 9, 20,
    Select * From movie_stars;
    Select title From movies Where (movie_id = 9 or movie_id = 20) and oscars > 2;

# 5. Show the Quentin Tarantino films which have more than 500000 votes and produced before 2000.
	Select director_id From directors Where director_name = "Quentin Tarantino"; #3
    Select movie_id From movie_directors Where director_id = 3; #4
    Select title From movies Where movie_id = 4 and votes > 500000 and year < 2000;
    
    Select title From movies join movie_directors on movies.movie_id = movie_directors.movie_id join directors on movie_directors.director_id = directors.
    director_id Where director_name = "Quentin Tarantino" and votes > 500000 and year < 2000;

# 6. Show the thriller films whose budget is greater than 25 million$.
	Select title From movies Where budget > 25000000 and movie_id in (Select movie_id From genres Where genre_name = "thriller");

# 7. Show the drama films whose language is Italian and produced between 1990-2000.	
	Select title, movie_id From movies Where year > 1990 and year < 2000 and movie_id in (Select movie_id From genres Where genre_name = "Drama");
    Select movie_id From languages Where language_name = "Italian";
    Select title From movies Where (year > 1990 and year < 2000) and ( movie_id = 2 or movie_id = 3 or movie_id = 5 or movie_id = 15) and movie_id in 
    (Select movie_id From genres Where genre_name = "Drama");
    
    Select title From movies join genres on movies.movie_id = genres.movie_id join languages on movies.movie_id = languages.movie_id Where genre_name = 
    "Drama" and language_name = "Italian" and year > 1990 and year < 2000;
    
# 8. Show the films that Tom Hanks has act and have won more than 3 Oscars.
	Select star_id From stars Where star_name = "Tom Hanks"; #47
    Select movie_id From movie_stars Where star_id = 47;
    Select title From movies Where movie_id = 18 and oscars > 3;
    
    Select title From movies join movie_stars on movies.movie_id = movie_stars.movie_id join stars on stars.star_id = movie_stars.star_id Where star_name
    = "Tom Hanks" and oscars > 3;

# 9. Show the history films produced in USA and whose duration is between 100-200 minutes.
	Select movie_id, title, duration From movies Where duration > 100 and duration < 200 and movie_id in (Select movie_id From genres Where genre_name = 
    "history");
    Select country_id From countries Where country_name = "USA";
    Select movie_id From producer_countries Where country_id = 1;
    
	Select title From movies join genres on movies.movie_id = genres.movie_id join producer_countries on movies.movie_id = producer_countries.movie_id 
    join countries on producer_countries.country_id = countries.country_id Where genre_name = "history" and country_name = "USA" and duration > 100 and 
    duration < 200;

# 10.Compute the average budget of the films directed by Peter Jackson.
	#SELECT AVG(column_name) FROM table_name WHERE condition;
    Select AVG(budget) From movies join movie_directors on movies.movie_id = movie_directors.movie_id join directors on movie_directors.director_id = 
    directors.director_id Where director_name = "Peter Jackson";
    Select AVG(budget) From movies;
    Select title, budget From movies join movie_directors on movies.movie_id = movie_directors.movie_id join directors on movie_directors.director_id = 
    directors.director_id Where director_name = "Peter Jackson";
    Select budget From movies;

# 11.Show the Francis Ford Coppola film that has the minimum budget.
	Select * From directors Where director_name = "Francis Ford Coppola";
    #SELECT MIN(column_name) FROM table_name WHERE condition;
    Select MIN(budget), title From movies join movie_directors on movies.movie_id = movie_directors.movie_id join directors on movie_directors.
    director_id = directors.director_id Where director_name = "Francis Ford Coppola";

# 12.Show the film that has the most vote and has been produced in USA.
	Select MAX(votes), title From movies join producer_countries on movies.movie_id = producer_countries.movie_id join countries on producer_countries.
    country_id = countries.country_id Where country_name = "USA";