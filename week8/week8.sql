Select * From shippers;

Select ShipperID, OrderID From orders Where ShipperID in (
	Select ShipperID From shippers);
    
Select ShipperID, Count(orderID) From orders Where ShipperID in (
	Select ShipperID From shippers);
    
Select ShipperName, Count(orderID) From orders Join shippers on orders.ShipperID = shippers.ShipperID group by shippers.ShipperID;

Select customerName, orderID From orders Join customers on orders.CustomerID = orders.CustomerID Order by OrderID;

Select customerName, orderID From customers Left Join orders on customer.CustomerID = orders.CustomerID Order by OrderID;

Select firstName, lastName, orderID From orders Join employees on orders.EmployeeID = employees.EmployeeID Order by OrderID;

Select firstName, lastName, orderID From orders Right Join employees on orders.EmployeeID = employees.EmployeeID Order by OrderID;


